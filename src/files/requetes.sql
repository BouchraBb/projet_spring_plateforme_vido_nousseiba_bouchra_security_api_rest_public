table mot_cle:
insert into mot_cle values(1,'ecologie');
insert into mot_cle values(2,'recyclage');
insert into mot_cle values(3,'energie');
insert into mot_cle values(4,'objet');


table liste_mot_cle :
insert into liste_mot_cle values(1,1);
insert into liste_mot_cle values(4,1);
insert into liste_mot_cle values(1,2);
insert into liste_mot_cle values(4,2);
insert into liste_mot_cle values(3,1);

récupérer les id videos des videos qui ont au moins un  mot clé commun avec notre video:
select distinct videos_id_video from liste_mot_cle where mot_cles_id_mot_cle in (select mot_cles_id_mot_cle from liste_mot_cle where videos_id_video=1)


récupérer les videos qui ont au moins un mot clé commun avec notre video:
select * from video where id_video in (
select distinct videos_id_video from liste_mot_cle where mot_cles_id_mot_cle in (select mot_cles_id_mot_cle from liste_mot_cle where videos_id_video=1))  limit 10;


insertion videos:
insert into video values(1,'01/01/2020','court metrage',null,null,'programmes_hd.mp4',null,2,null,'',null,null);
insert into video values(2,'01/01/2020','court metrage',null,null,'connectes_hd.mp4',null,2,null,'',null,null);
insert into video values(3,'01/01/2020','court metrage',null,null,'demesures_hd.mp4',null,2,null,'',null,null);

insertion liste_lecure:
insert into liste_lecture values(1,1);
insert into liste_lecture values(1,1);

insertion images:
insert into image values(1,'true','programme.jpg', null, 1);
insert into image values(2,'true','energie3.jpg', null, 2);
insert into image values(3,'true','demesures.jpg', null, 3);
insert into image values(4,'true','fleurs.jpg', null, 4);