package fr.afpa.plateformevideo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "Historique.userHistorique", query = "select h from Historique h where h.utilisateur.idUser = ?1 ")
public class Historique {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idHistorique;
    private Timestamp dateHistorique;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idVideo")
    Video video;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    User utilisateur;


    public Historique(Timestamp dateHistorique, Video video, User utilisateur) {
        this.dateHistorique = dateHistorique;
        this.video = video;
        this.utilisateur = utilisateur;
    }

    @Override
    public String toString() {
        return "Historique{" +
                "idHistorique=" + idHistorique +
                ", dateHistorique=" + dateHistorique +
                '}';
    }
}
