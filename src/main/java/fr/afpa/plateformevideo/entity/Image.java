package fr.afpa.plateformevideo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = "Image.miniatureVideo", query = "select a from Image a where a.video.idVideo = ?1 and a.miniature is true")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idImage;
    private String nom;
    private String url;

    private Boolean miniature;

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "idVideo")
    Video video;


    @Override
    public String toString() {
        return "Image{" +
                "idImage=" + idImage +
                ", nom='" + nom + '\'' +
                ", url='" + url + '\'' +
                ", miniature=" + miniature +
                '}';
    }
}
