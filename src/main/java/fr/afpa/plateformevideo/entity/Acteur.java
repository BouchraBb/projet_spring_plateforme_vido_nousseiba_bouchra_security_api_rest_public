package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Acteur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idActeur;
    private String nom;
    private String prenom;

    @ManyToMany
    @JoinTable(name = "listeVideo")
    List<Video> videos;

}
