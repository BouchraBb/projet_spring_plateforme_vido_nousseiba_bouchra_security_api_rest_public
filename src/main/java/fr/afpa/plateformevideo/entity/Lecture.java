package fr.afpa.plateformevideo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({ @NamedQuery(name = "Lecture.allUserVideoLecture", query = "select l from Lecture l where l.utilisateur.idUser = ?1 "),
        @NamedQuery(name = "Lecture.userVideoLecture", query = "select l from Lecture l where l.utilisateur.idUser = ?1 and l.video.idVideo= ?2 ")})
public class Lecture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idLecture;

    private Integer timePause;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "idVideo")
    Video video;

    @ManyToOne
    @JoinColumn(name = "idUtilisateur")
    User utilisateur;

    public Lecture(Video video, User utilisateur) {
        this.video = video;
        this.utilisateur = utilisateur;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "idLecture=" + idLecture +
                ", timePause=" + timePause +
                ", videoId=" + video.getIdVideo() +
                ", utilisateurId=" + utilisateur.getIdUser() +
                '}';
    }
}
