package fr.afpa.plateformevideo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@NamedQueries({@NamedQuery(name = "User.findByEmailAndPassword",
        query = "select u from User u where u.email = ?1 and u.password= ?2"),
        @NamedQuery(name="Video.favoritsVideo", query= "SELECT u FROM User u JOIN u.videos v WHERE u.idUser = :idUser")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq")
    @SequenceGenerator(name = "users_seq", sequenceName = "users_seq", allocationSize = 1)
    private Integer idUser;


    @Column(unique = true, nullable = false)
    private String username;
    @Column(unique = true, nullable = false)
    private String email;
    @JsonIgnore
    @Column(unique = true, nullable = false)
    private String password;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_authority",
            joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "idUser") },
            inverseJoinColumns = { @JoinColumn(name = "authority_name", referencedColumnName = "name") }
    )
    private Set<Authority> authorities = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    private List<Critique> critiques =new ArrayList<>();

    @OneToMany(mappedBy = "utilisateur")
    private List<Abonnement> abonnements =new ArrayList<>();

    @OneToMany(mappedBy = "utilisateur")
    private List<Historique> historiques =new ArrayList<>();
    @OneToMany(mappedBy = "utilisateur")
    private List<Lecture> lectures =new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "favorie",  uniqueConstraints = @UniqueConstraint(columnNames = {"utilisateurs_id_user", "videos_id_video"}))
    List<Video> videos=new ArrayList<>();


    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;



    }

    @Override
    public String toString() {
        return "UtilisateurDTO{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +

                '}';
    }

}
