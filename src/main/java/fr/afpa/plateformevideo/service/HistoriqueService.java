package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Historique;

import java.util.List;

public interface HistoriqueService {

    public Historique create(Historique historique);
    public List<Historique> findAll();

    public Historique findById(Integer id);

    public void update(Historique historique);

    public void delete(Historique historique);
    public List<Historique> userHistorique(Integer idUtilisateur);
}
