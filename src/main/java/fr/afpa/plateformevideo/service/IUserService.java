package fr.afpa.plateformevideo.service;


import fr.afpa.plateformevideo.dto.UserDTO;
import fr.afpa.plateformevideo.dto.UserSecurity;
import fr.afpa.plateformevideo.entity.User;
import fr.afpa.plateformevideo.entity.Video;

import java.util.List;


public interface IUserService {

    Integer save(UserSecurity user);
    UserDTO findOneByUsername(String username);

    public UserDTO create(User utilisateur);
    public List<User> findAll();

    public UserDTO findById(Integer id);
    public User findByUsername(String username);

    User findById1(Integer id);

    public void update(UserDTO utilisateur);

    public void delete(User utilisateur);

    void addVideoToHistorique(UserDTO user, Integer id);

    void addVideoToFavorit(UserDTO user,  Integer id);

    void removeVideoFromFavorit(UserDTO user, Integer id);

    List<Video> userFavorit(Integer idUser);


}
