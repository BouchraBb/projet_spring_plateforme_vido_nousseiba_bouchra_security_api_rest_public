package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Serie;

import java.util.List;

public interface SerieService {

    public Serie create(Serie serie);
    public List<Serie> findAll();

    public Serie findById(Integer id);

    public void update(Serie serie);

    public void delete(Serie serie);
}
