package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Description;

import java.util.List;

public interface DescriptionService {

    public Description create(Description description);
    public List<Description> findAll();

    public Description findById(Integer id);

    public void update(Description description);

    public void delete(Description description);
}
