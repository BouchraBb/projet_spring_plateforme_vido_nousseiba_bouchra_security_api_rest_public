package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Acteur;

import java.util.List;

public interface ActeurService {

    public Acteur create(Acteur acteur);
    public List<Acteur> findAll();

    public Acteur findById(Integer id);

    public void update(Acteur acteur);

    public void delete(Acteur acteur);
}
