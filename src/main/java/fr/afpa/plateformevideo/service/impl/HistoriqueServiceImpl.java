package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.repository.HistoriqueRepository;
import fr.afpa.plateformevideo.service.HistoriqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoriqueServiceImpl implements HistoriqueService {

    @Autowired
    HistoriqueRepository HistoriqueRepository;


    @Override
    public Historique create(Historique historique) {
        return HistoriqueRepository.save(historique);
    }

    @Override
    public List<Historique> findAll() {
        return HistoriqueRepository.findAll();
    }

    @Override
    public Historique findById(Integer id) {
        return HistoriqueRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Historique historique) {
        HistoriqueRepository.saveAndFlush(historique);
    }

    @Override
    public void delete(Historique historique) {
        HistoriqueRepository.delete(historique);
    }

    @Override
    public List<Historique> userHistorique(Integer idUtilisateur) {
        return HistoriqueRepository.userHistorique(idUtilisateur);
    }
}
