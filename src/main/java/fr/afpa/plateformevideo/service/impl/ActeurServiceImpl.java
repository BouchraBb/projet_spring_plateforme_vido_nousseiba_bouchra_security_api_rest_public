package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Acteur;
import fr.afpa.plateformevideo.repository.ActeurRepository;
import fr.afpa.plateformevideo.service.ActeurService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ActeurServiceImpl implements ActeurService {
    @Autowired
    ActeurRepository acteurRepository;
    @Override
    public Acteur create(Acteur acteur) {
        return acteurRepository.save(acteur);
    }

    @Override
    public List<Acteur> findAll() {
        return acteurRepository.findAll();
    }

    @Override
    public Acteur findById(Integer id) {
        return acteurRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Acteur acteur) {
        acteurRepository.saveAndFlush(acteur);
    }

    @Override
    public void delete(Acteur acteur) {
        acteurRepository.delete(acteur);

    }
}
