package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.MotCle;
import fr.afpa.plateformevideo.repository.MotCleRepository;
import fr.afpa.plateformevideo.service.MotCleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MotCleServiceimpl implements MotCleService {
    @Autowired
    MotCleRepository motCleRepository;
    @Override
    public MotCle create(MotCle motCle) {
        return motCleRepository.save(motCle);
    }

    @Override
    public List<MotCle> findAll() {
        return motCleRepository.findAll();
    }

    @Override
    public MotCle findById(Integer id) {
        return motCleRepository.findById(id).orElse(null);
    }

    @Override
    public void update(MotCle motCle) {
        motCleRepository.saveAndFlush(motCle);

    }

    @Override
    public void delete(MotCle motCle) {
        motCleRepository.delete(motCle);
    }
}
