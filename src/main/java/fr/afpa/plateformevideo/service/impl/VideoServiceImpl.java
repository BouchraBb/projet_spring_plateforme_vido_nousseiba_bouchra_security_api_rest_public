package fr.afpa.plateformevideo.service.impl;

import com.google.cloud.storage.*;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.VideoRepository;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.VideoService;
import fr.afpa.plateformevideo.utils.Constantes;
import fr.afpa.plateformevideo.utils.CustomMultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    VideoRepository VideoRepository;
    @Autowired
    ImageService imageService;

    String bucketName = Constantes.BUCKETNAME;

    Storage storage= ImageServiceImpl.storage;


    @Override
    public Video create(Video video) {
        return VideoRepository.save(video);
    }

    @Override
    public List<Video> findAll() {
        return VideoRepository.findAll();
    }

    @Override
    public Video findById(Integer id) {
        return VideoRepository.findById(id).orElse(null);
    }

    @Override
    public Video update(Video video) {
        VideoRepository.saveAndFlush(video);
        return  video;
    }

    @Override
    public void delete(Video video) {
        VideoRepository.delete(video);
    }


    @Override
    public byte[] videoToByte(Video video) {
        BlobId blobId = BlobId.of(bucketName,video.getTitre()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
        Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP

        return blob.getContent();
    }

    @Override
    public List<Video> findVideosSimilaires(int idVideo) {
        return VideoRepository.videosSimilaires(idVideo);
    }


    @Override
    public byte[][] listVideosToByte(List<Video> videoList) {

        byte[][] videos = new byte[videoList.size()][];
        int cpt = 0;
        for (Video video : videoList) {

            BlobId blobId = BlobId.of(bucketName, video.getTitre()); // remplacez my-bucket et video.mp4 par le nom de votre bucket et le nom de votre vidéo
            //System.out.println("blobId: " + blobId);

            Blob blob = storage.get(blobId); // récupérez la vidéo à partir de GCP
            if(blob!=null) {
                //videos[cpt] = new byte[blob.getContent().length];
                //System.out.println("blob: " + blob);
                //System.out.println("cpt: " + cpt);
                videos[cpt] = blob.getContent();
                cpt++;
            }

        }
        //System.out.println("cpt"+ cpt);

        return videos;
    }

    @Override
    public List<Video> setByteMiniatureForListVideos(List<Video> listVideos ) {
            byte[][] miniatures=new byte[listVideos.size()][];
            for (int i=0; i<listVideos.size();i++){
                miniatures[i]= imageService.miniatureToByte(listVideos.get(i).getIdVideo());
                listVideos.get(i).setByteMiniature(miniatures[i]);
                update(listVideos.get(i));
            }
            return listVideos;
        }

    @Override
    public String uploadVideoIntoBucket(MultipartFile file) {
        String name=null;
        try {
            System.out.println("file" + file.getName());
            String fileName = file.getOriginalFilename();
            InputStream inputStream = file.getInputStream();
            BlobId blobId = BlobId.of(bucketName, fileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("video/mp4").build();
            name =   storage.create(blobInfo, inputStream.readAllBytes()).getName();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    @Override
    public void deleteVideoIntoBucket(String fileName) {
        try {
            BlobId blobId = BlobId.of(bucketName, fileName);
            storage.delete(blobId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public byte[] downloadVideoFromBucket(String fileName) {
        try {
            // Créez une instance de service Google Cloud Storage
            System.err.println("storage " + storage);
            System.err.println("fileName " + fileName);
            // Définissez le nom du bucket et le chemin du fichier dans Google Cloud Storage
            String bucketName = Constantes.BUCKETNAME;
            String filePath = Constantes.BUCKETURL + fileName;
            System.err.println("filePath " + filePath);


            // Téléchargez le fichier depuis Google Cloud Storage
            BlobId blobId = BlobId.of(bucketName, fileName);
            System.err.println("blobid " + blobId);
            Blob blob = storage.get(blobId);
            System.err.println("blob " + blob);
            //BlobInfo blobInfo = blob.getBlobId().toBuilder().build();
            byte[] content = blob.getContent();
            //MultipartFile multipartFile = new CustomMultipartFile(fileName, content);


            // Créez une ressource Spring à partir du flux du fichier
            //ByteArrayResource resource = new ByteArrayResource(content);
            return content;


            // Retournez une réponse avec le fichier en tant que contenu

        } catch (Exception e) {
            // Gérer les erreurs de téléchargement ici
            e.printStackTrace();
            return null;

        }
    }

    @Override
    public CustomMultipartFile byteToMultipart(byte[] file) {
        return null;
    }

/*    @Override
        public CustomMultipartFile byteToMultipart(byte[] file) {
      *//*      //byte[] inputArray = "Test String".getBytes();
            CustomMultipartFile customMultipartFile = new CustomMultipartFile();*//*
            return customMultipartFile;

        }*/

    }


