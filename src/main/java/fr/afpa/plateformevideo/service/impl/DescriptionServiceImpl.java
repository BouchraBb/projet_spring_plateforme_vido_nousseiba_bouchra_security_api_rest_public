package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Description;
import fr.afpa.plateformevideo.repository.DescriptionRepository;
import fr.afpa.plateformevideo.service.DescriptionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DescriptionServiceImpl implements DescriptionService {
    @Autowired
    DescriptionRepository descriptionRepository;
    @Override
    public Description create(Description description) {
        return descriptionRepository.save(description);
    }

    @Override
    public List<Description> findAll() {
        return descriptionRepository.findAll();
    }

    @Override
    public Description findById(Integer id) {
        return descriptionRepository.findById(id).orElse(null );
    }

    @Override
    public void update(Description description) {
        descriptionRepository.saveAndFlush(description);

    }

    @Override
    public void delete(Description description) {
        descriptionRepository.delete(description);

    }
}
