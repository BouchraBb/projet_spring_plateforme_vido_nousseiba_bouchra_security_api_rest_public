package fr.afpa.plateformevideo.service.impl;

import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.repository.CritiqueRepository;
import fr.afpa.plateformevideo.service.CritiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CritiqueServiceImpl implements CritiqueService {
    @Autowired
    CritiqueRepository CritiqueRepository;


    @Override
    public Critique create(Critique critique) {
        return CritiqueRepository.save(critique);

    }

    @Override
    public List<Critique> findAll() {
        return CritiqueRepository.findAll();
    }

    @Override
    public Critique findById(Integer id) {
        return CritiqueRepository.findById(id).orElse(null);
    }

    @Override
    public void update(Critique critique) {
        CritiqueRepository.saveAndFlush(critique);
    }

    @Override
    public void delete(Critique critique) {
        CritiqueRepository.delete(critique);
    }


    @Override
    public List<Critique> getCritiquesByVideo(int idVideo) {
        return CritiqueRepository.critiquesByVideo(idVideo);
    }

    @Override
    public Double scoreByVideo(Integer idVideo) {
        return CritiqueRepository.scoreByVideo(idVideo);
    }


}
