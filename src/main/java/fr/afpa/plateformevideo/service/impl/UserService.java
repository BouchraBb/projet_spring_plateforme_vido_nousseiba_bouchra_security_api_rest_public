package fr.afpa.plateformevideo.service.impl;


import fr.afpa.plateformevideo.dto.UserDTO;
import fr.afpa.plateformevideo.dto.UserSecurity;
import fr.afpa.plateformevideo.entity.Authority;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.User;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.repository.IUserRepository;
import fr.afpa.plateformevideo.repository.VideoRepository;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.HistoriqueService;
import fr.afpa.plateformevideo.service.IUserService;
import fr.afpa.plateformevideo.service.VideoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    VideoRepository videoRepository;
    @Autowired
    VideoService videoService;
    @Autowired
    HistoriqueService historiqueService;
    @Autowired
    private IUserRepository userRepository;

    // password encode permet d'encoder le mot de passe avant de l'enregistrer en BDD
    // Pour fonctionner, PasswordEncoder est défini dans SecurityConfig (dans le package Security)
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Integer save(UserSecurity userSecurity) {
        User user = new User(userSecurity.getUsername(),userSecurity.getEmail(),
        passwordEncoder.encode(userSecurity.getPassword())); //On encode le password avec PasswordEncoder
        if(userSecurity.getAuthorities() == null || userSecurity.getAuthorities().size() == 0) {
            Authority authority = new Authority(AuthorityConstant.ROLE_USER);
            Set<Authority> authorities = new HashSet<>();
            authorities.add(authority);
            user.setAuthorities(authorities);
        }
        else{

            user.setAuthorities(userSecurity.getAuthorities());

        }
        return this.userRepository.save(user).getIdUser();
    }

    @Override
    public UserDTO findOneByUsername(String username) {
        return userRepository.findOneByUsername(username);
    }

    @Override
    public UserDTO create(User utilisateur) {
        return modelMapper.map( userRepository.save(utilisateur),UserDTO.class) ;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDTO findById(Integer id) {
        return  modelMapper.map(userRepository.findById(id).orElse(null), UserDTO.class);
    }
    @Override
    public User findByUsername(String username) {
        return  userRepository.findByUsername(username);
    }

    @Override
    public User findById1(Integer id) {
        return  userRepository.findById(id).orElse(null);
    }

    @Override
    public void update(UserDTO utilisateur) {
        userRepository.saveAndFlush( modelMapper.map(utilisateur, User.class));
    }

    private void update1(User utilisateur) {
        userRepository.saveAndFlush( utilisateur);
    }

    @Override
    public void delete(User utilisateur) {
        userRepository.delete(utilisateur);
    }


    @Override
    public void addVideoToHistorique(UserDTO user, Integer id) {
        Video video = videoService.findById(id);
        User utilisateur  = findById1(user.getIdUser());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        java.sql.Timestamp date = new java.sql.Timestamp(calendar.getTime().getTime());

        Historique historique= new Historique(date, video, utilisateur );
        historiqueService.create(historique);
        utilisateur.getHistoriques().add(historique);
        update1(utilisateur);
    }

    @Override
    public void addVideoToFavorit(UserDTO user,  Integer id) {
        Video video = videoService.findById(id);
        User utilisateur = findById1(user.getIdUser());
        utilisateur.getVideos().add(video);
        update1(utilisateur);
    }

    @Override
    public void removeVideoFromFavorit(UserDTO user, Integer id) {
        Video video = videoService.findById(id);
        User utilisateur = findById1(user.getIdUser());
       utilisateur.getVideos().remove(video);
        update1(utilisateur);
    }

    @Override
    public List<Video> userFavorit(Integer idUtilisateur) {
        return videoRepository.findFavoritesByUserId(idUtilisateur);
    }


}
