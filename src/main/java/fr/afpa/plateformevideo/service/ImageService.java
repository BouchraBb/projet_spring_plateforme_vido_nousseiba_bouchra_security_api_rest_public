package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {
    public Image create(Image image);
    public List<Image> findAll();

    public Image findById(Integer id);

    public Image update(Image image);

    public void delete(Image image);



    byte[][] allImagesByVideoToByte(Integer id);

    byte[][] allImagesToByte();

    String uploadImageToCloud(MultipartFile file);

    Image findMiniatureVideo(Integer id);

    byte[] imageToByte(Image image);

    byte[] miniatureToByte(Integer id);
}
