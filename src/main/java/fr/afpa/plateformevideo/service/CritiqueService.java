package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Critique;

import java.util.List;

public interface CritiqueService {

    public Critique create(Critique critique);
    public List<Critique> findAll();

    public Critique findById(Integer id);

    public void update(Critique critique);

    public void delete(Critique critique);

    List<Critique> getCritiquesByVideo(int idVideo);

    Double scoreByVideo(Integer idVideo);
}
