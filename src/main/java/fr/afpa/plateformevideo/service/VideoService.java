package fr.afpa.plateformevideo.service;

import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.utils.CustomMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VideoService {

    public Video create(Video video);
    public List<Video> findAll();

    public Video findById(Integer id);

    public Video update(Video video);

    public void delete(Video video);

    byte[] videoToByte(Video video);

    List<Video> findVideosSimilaires(int idVideo);

    byte[][] listVideosToByte(List<Video> videoList);

    List<Video> setByteMiniatureForListVideos(List<Video> all);

    String uploadVideoIntoBucket(MultipartFile file);

    void deleteVideoIntoBucket(String fileName);

    byte[] downloadVideoFromBucket(String fileName);

    CustomMultipartFile byteToMultipart(byte[] file);
}
