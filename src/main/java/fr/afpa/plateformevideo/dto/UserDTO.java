package fr.afpa.plateformevideo.dto;

import fr.afpa.plateformevideo.entity.Abonnement;
import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.Historique;
import fr.afpa.plateformevideo.entity.Video;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDTO {

    private Integer idUser;
    private String username;
    private String email;
    private List<String> authorities;
    private List<Critique> critiques =new ArrayList<>();
    private List<Abonnement> abonnements =new ArrayList<>();
    private List<Historique> historiques =new ArrayList<>();
    private List<Video> videos =new ArrayList<>();
    public <R> UserDTO(Integer idUser, String username, R collect) {
    }

    @Override
    public String toString() {
        return "UtilisateurDTO{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
