package fr.afpa.plateformevideo.dto;

import fr.afpa.plateformevideo.entity.Authority;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserSecurity {

    private String username;
    private String email;
    private String password;
    private Set<Authority> authorities;

}
