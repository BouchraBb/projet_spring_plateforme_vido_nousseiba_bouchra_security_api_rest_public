package fr.afpa.plateformevideo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class VideoInfosDTO {

        private Integer idVideo;
        private String titre;
        private Integer duree;
        private String genre;
        private String url;
        private Integer numSaison;
        private Integer numEpisode;


        public VideoInfosDTO(String titre, Integer duree, String genre, String url, Integer numSaison, Integer numEpisode) {
            this.titre = titre;
            this.duree = duree;
            this.genre = genre;
            this.url = url;
            this.numSaison = numSaison;
            this.numEpisode = numEpisode;
        }

    }

