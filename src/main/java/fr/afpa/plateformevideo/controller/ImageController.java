package fr.afpa.plateformevideo.controller;

import com.google.cloud.storage.*;
import fr.afpa.plateformevideo.entity.Image;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.ImageService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.plateformevideo.utils.*;

import java.util.List;

@RequestMapping("/plateformeVideo/v2/api/image/")
@RestController
public class ImageController {

    @Autowired
    ImageService imageService;
    @Autowired
    VideoService videoService;



    @Secured({ AuthorityConstant.ROLE_ADMIN})
    @PostMapping("upload")
    public ResponseEntity<Image> upload(@RequestParam("file") MultipartFile file) {
        Image image= new Image();
        String name = imageService.uploadImageToCloud(file);
        image.setNom(name);
        Image imageCreated = imageService.create(image);
        return new ResponseEntity<>(imageCreated, HttpStatus.OK);
    }
    @Secured({ AuthorityConstant.ROLE_ADMIN})
    @PutMapping("update")
    public ResponseEntity<Image> upload(@RequestBody Image image) {
        imageService.update(image);
        Image imageCreated = imageService.update(image);
        return new ResponseEntity<>(imageCreated, HttpStatus.OK);
    }

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/miniatureByte/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> findMiniatureByteByVideo(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        byte[] miniature = imageService.miniatureToByte(idVideo);
        return new ResponseEntity<>(miniature, HttpStatus.OK);
    }


    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/images/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Image>> findAllImagesByVideo(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        List<Image> imageList = video.getImages();
        return new ResponseEntity<>(imageList, HttpStatus.OK);
    }


    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/imagesByte/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[][]> findAllImagesByteByVideo(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        byte[][] images = imageService.allImagesByVideoToByte(idVideo);
        return new ResponseEntity<>(images, HttpStatus.OK);
    }


   /* @RequestMapping(value = "show/all", method = RequestMethod.GET)
    public ModelAndView showAll() throws Exception {
        byte[][] images=imageService.allImagesToByte();

        ModelAndView mav = new ModelAndView("jsp/images"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("imageData", images); // ajoutez les données de la vidéo à l'objet ModelAndView
        return mav; // retournez l'objet ModelAndView à la vue JSP
    }*/

}
