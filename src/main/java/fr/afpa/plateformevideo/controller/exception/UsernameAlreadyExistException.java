package fr.afpa.plateformevideo.controller.exception;

public class UsernameAlreadyExistException extends RuntimeException {

    public UsernameAlreadyExistException() {
        super("Username already exist");
    }

}
