package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.dto.UserDTO;
import fr.afpa.plateformevideo.entity.Critique;
import fr.afpa.plateformevideo.entity.User;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.CritiqueService;
import fr.afpa.plateformevideo.service.IUserService;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@RequestMapping("/plateformeVideo/v2/api/video/critique")
@Controller
public class CritiqueController {

    @Autowired
    CritiqueService critiqueService;
    @Autowired
    VideoService videoService;
    @Autowired
    IUserService utilisateurService;

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Critique> findVideoById(@PathVariable("id") Integer idCritique)  {
        Critique critique = critiqueService.findById(idCritique);
        return new ResponseEntity<>(critique, HttpStatus.OK);
    }

    @Secured({AuthorityConstant.ROLE_USER})
    @PostMapping("/add/{idVideo}/{idUser}")
    public ResponseEntity<Critique> ajouterCritiqueVideo(@PathVariable("idVideo") Integer idVideo,@PathVariable("idUser") Integer idUser, @RequestBody Critique critique) {

        User user=utilisateurService.findById1(idUser);
        critique.setDateCritique(Date.valueOf(LocalDate.now()));
        critique.setVideo(videoService.findById(idVideo));
        critique.setUtilisateur(user);
        Critique critique1=critiqueService.create(critique);

        return new ResponseEntity<>(critique1, HttpStatus.OK);

    }

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/score/{idVideo}", method = RequestMethod.GET)
    public ResponseEntity<Double> scoreVideo(@PathVariable("idVideo") Integer idVideo) {
        Double scoreVideo = critiqueService.scoreByVideo(idVideo);
        return new ResponseEntity<>(scoreVideo, HttpStatus.OK);
    }


    @RequestMapping("/allForVideo/{idVideo}")
    public ResponseEntity<List<Critique> > getCritiquesByVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id ) {
        List<Critique> critiquesVideo = critiqueService.getCritiquesByVideo(id);
        return new ResponseEntity<>(critiquesVideo, HttpStatus.OK);

    }

    @RequestMapping("/hideCritiquesByVideo/{idVideo}")
    public String hideCritiquesByVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id ) {
        model.addAttribute("critiquesVideo", null);

        return "jsp/detailsvideo";

    }




}
