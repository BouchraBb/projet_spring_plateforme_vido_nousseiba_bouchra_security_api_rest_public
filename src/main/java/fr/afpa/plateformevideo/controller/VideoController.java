package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.entity.*;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@RequiredArgsConstructor
@RestController

@RequestMapping("/plateformeVideo/v2/api/video")
public class VideoController {

    @Autowired
    VideoService videoService;

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Video>> allVideos(){
        List<Video> allVideos = videoService.findAll();

        if (allVideos != null) {
            return new ResponseEntity<>(allVideos, HttpStatus.OK);
        }
        return new ResponseEntity<>(allVideos, HttpStatus.NO_CONTENT);
    }

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Video> findVideoById(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        return new ResponseEntity<>(video, HttpStatus.OK);
    }
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/videoByte/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> findVideoByteById(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        byte[] videoByte = videoService.videoToByte(video);
        return new ResponseEntity<>(videoByte, HttpStatus.OK);
    }


    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/videosSimilaires/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Video>> findVideosSimilaires(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        List<Video> listVideos = videoService.findVideosSimilaires(idVideo);
        return new ResponseEntity<>(listVideos, HttpStatus.OK);
    }


    @Secured({ AuthorityConstant.ROLE_ADMIN})
    @PostMapping("/upload")
    public ResponseEntity<Video> upload(@RequestBody MultipartFile file) {
            Video video= new Video();
            String name = videoService.uploadVideoIntoBucket(file);
            video.setTitre(name);
            Video video2 = videoService.create(video);
            return new ResponseEntity<>(video2, HttpStatus.OK);
    }

    @Secured({ AuthorityConstant.ROLE_ADMIN})
    @PutMapping("update")
    public ResponseEntity<Video> update(@RequestBody Video video) {
        videoService.update(video);
        Video video1 = videoService.update(video);
        return new ResponseEntity<>(video1, HttpStatus.OK);
    }


    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("/download/{idVideo}")
    public byte[] downloadVideoFromBucket(@PathVariable("idVideo") Integer idVideo) {
        Video video=videoService.findById(idVideo);
        return videoService.downloadVideoFromBucket(video.getTitre());
    }

    //Erreur dans postman : Maximum response size reached
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/show/all", method = RequestMethod.GET)
    public ResponseEntity<byte[][]> showAll() throws Exception {
        List<Video> allVideos = videoService.findAll();
        byte[][] videoList = new byte[0][];
        if (allVideos != null) {
            videoList = videoService.listVideosToByte(allVideos);
            return new ResponseEntity<>(videoList, HttpStatus.OK);
        }
        return new ResponseEntity<>(videoList, HttpStatus.NO_CONTENT);
    }













    //todo download une video en format multipart file
/*    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("/download2/{idVideo}")
    public MultipartFile downloadVideoFromBucket2(@PathVariable("idVideo") Integer idVideo) {
        Video video=videoService.findById(idVideo);
        MultipartFile multipartFile=videoService.downloadVideoFromBucket(video.getTitre());
        return multipartFile;
    }*/



/*


    //todo a corriger
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "/favoris/ajouter/{idVideo}/{idUser}", method = RequestMethod.GET)
    public ResponseEntity addVideoToFavoris(@PathVariable("idVideo") Integer idVideo, @PathVariable("idUser") Integer idUser) {
        UtilisateurDTO user= utilisateurService.findById(idUser);
        utilisateurService.addVideoToFavorit(user, idVideo);
        utilisateurService.update(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //todo à corriger
    @RequestMapping(value = "/favoris/retirer/{idVideo}/{idUser}", method = RequestMethod.GET)
    public ResponseEntity removeVideoFromFavoris(@PathVariable("idVideo") Integer idVideo,@PathVariable("idUser") Integer idUser) {
        UtilisateurDTO user= utilisateurService.findById(idUser);
        utilisateurService.removeVideoFromFavoris(user, idVideo);
        utilisateurService.update(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }













    @RequestMapping(value = "/lireVideo/{idVideo}", method = RequestMethod.GET)
    public String lireVideo(Model model, HttpServletRequest request, @PathVariable("idVideo") Integer id) {
        Video video = videoService.findById(id);
        UserDTO user = (UserDTO) request.getSession().getAttribute("utilisateur");

        utilisateurService.addVideoToHistorique(user, id);
        lectureService.addVideoToLecture(user, id);
        video.ajouterUneVue();

        Lecture lecture = lectureService.userVideoLecture(user.getIdUser(), id);
        request.getSession().setAttribute("timePause", lecture.getTimePause());

        videoService.update(video);
        request.getSession().setAttribute("video", video);
        return "jsp/lirevideo";
    }


    @PostMapping("/upload/video")
    public String uploadVideo(@RequestParam("file") MultipartFile file, @RequestParam("miniature") MultipartFile miniature, @ModelAttribute("video") Video video, BindingResult result) {

        if (!result.hasErrors()) {
            String name = videoService.uploadVideoIntoBucket(file);
            video.setTitre(name);

            Image min = imageService.uploadImageToCloud(miniature);
            min.setMiniature(true);

            video.getImages().add(min);

            Video video2 = videoService.create(video);
            min.setVideo(video2);
            imageService.update(min);

            byte[] minByte = imageService.miniatureToByte(video2.getIdVideo());
            video2.setByteMiniature(minByte);
            videoService.update(video2);
            //videoService.update(vid);
        }

        return "jsp/saveVideo";
    }




    @RequestMapping(value = "/admin/updateVideos", method = RequestMethod.GET)
    public ModelAndView setByteMiniatureVideos() throws Exception {
        List<Video> listVideos = videoService.findAll();
        //byte[][] videoList=videoService.showVideos(listVideos);
        byte[][] miniatures = new byte[listVideos.size()][];
        for (int i = 0; i < listVideos.size(); i++) {
            miniatures[i] = imageService.miniatureToByte(listVideos.get(i).getIdVideo());
            listVideos.get(i).setByteMiniature(miniatures[i]);
            videoService.update(listVideos.get(i));
        }

        ModelAndView mav = new ModelAndView("jsp/modifierVideos"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("videoData", listVideos); // ajoutez les données de la vidéo à l'objet ModelAndView

        return mav; // retournez l'objet ModelAndView à la vue JSP
    }

    @RequestMapping(value = "/updateOneVideo/{idVideo}", method = RequestMethod.GET)
    public String updateOneVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id, Model model) {
        Video video = videoService.findById(id);
        model.addAttribute("video", video);
        return "jsp/modifierUneVideo";
    }


    @PostMapping("/updateOneVideo/{idVideo}")
    public String updateOneVideo(HttpServletRequest request, Model model, @PathVariable("idVideo") Integer id, @RequestParam("miniature") MultipartFile miniature, @ModelAttribute("video") Video video) {
        //Video video1= videoService.findById(id);
        //videoService.update(video);
        //todo trouver miniature de la video et la remplacer par la nouvelle miniature
        Image minAncien = imageService.findMiniatureVideo(id);
        if (minAncien != null) {
            minAncien.setMiniature(false);
            imageService.update(minAncien);
        }
        //todo remplacer miniatureByte par la nouvelle
        Image min = imageService.uploadImageToCloud(miniature);

        min.setMiniature(true);
        video.getImages().add(min);

        min.setVideo(video);
        imageService.update(min);

        byte[] minByte = imageService.miniatureToByte(video.getIdVideo());
        video.setByteMiniature(minByte);
        videoService.update(video);


        //request.getSession().setAttribute("miniature", minByte);
        model.addAttribute("video", video);
        return "jsp/detailsvideo";
    }


    @RequestMapping(value = "/admin/deleteVideo/{idVideo}", method = RequestMethod.GET)
    public ModelAndView deleteOneVideo(HttpServletRequest request, @PathVariable("idVideo") Integer id, Model model) {
        Video video = videoService.findById(id);
        String name = video.getTitre();

        videoService.deleteVideoIntoBucket(name);
        Image image=imageService.findMiniatureVideo(id);
        imageService.delete(image);

        List<Critique> critiques=critiqueService.getCritiquesByVideo(id);
        if(critiques.size()>0) {
            for (Critique critique : critiques
            ) {
                critiqueService.delete(critique);
            }
        }
        videoService.delete(video);
        List<Video> listVideos = videoService.findAll();
        listVideos = videoService.setByteMiniatureForListVideos(listVideos);

        ModelAndView mav = new ModelAndView("jsp/modifierVideos"); // créez un objet ModelAndView avec la vue JSP nommée "video"
        mav.addObject("videoData", listVideos); // ajoutez les données de la vidéo à l'objet ModelAndView

        return mav; // retournez l'objet ModelAndView à la vue JSP
    }

*/

}


