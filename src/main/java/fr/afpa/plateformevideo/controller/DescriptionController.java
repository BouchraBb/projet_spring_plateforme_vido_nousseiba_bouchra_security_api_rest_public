package fr.afpa.plateformevideo.controller;

import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@RequestMapping("/plateformeVideo/v2/api/video/description/")
@Controller
public class DescriptionController {
    @Autowired
    VideoService videoService;
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @RequestMapping(value = "find/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> findDescriptionVideo(@PathVariable("id") Integer idVideo)  {
        Video video = videoService.findById(idVideo);
        if(video==null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        String description=video.getDescription();
        return new ResponseEntity<>(description, HttpStatus.OK);
    }
}
