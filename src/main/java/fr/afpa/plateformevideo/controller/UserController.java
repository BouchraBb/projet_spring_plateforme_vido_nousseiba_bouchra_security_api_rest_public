package fr.afpa.plateformevideo.controller;


import fr.afpa.plateformevideo.controller.exception.UsernameAlreadyExistException;
import fr.afpa.plateformevideo.dto.UserDTO;
import fr.afpa.plateformevideo.dto.UserSecurity;
import fr.afpa.plateformevideo.entity.User;
import fr.afpa.plateformevideo.entity.Video;
import fr.afpa.plateformevideo.security.AuthorityConstant;
import fr.afpa.plateformevideo.service.IUserService;
import fr.afpa.plateformevideo.service.VideoService;
import fr.afpa.plateformevideo.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@RequestMapping("/plateformeVideo/v2/api/user")
@RestController
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    UserService utilisateurService;
    @Autowired
    VideoService videoService;

    @PostMapping("/register")
    public ResponseEntity<Integer> register(@RequestBody UserSecurity userSecurity) {
        UserDTO userDTO = userService.findOneByUsername(userSecurity.getUsername());
        if(userDTO != null) {
            throw new UsernameAlreadyExistException();
        }
        Integer idSaved = userService.save(userSecurity);
        return new ResponseEntity<>(idSaved, HttpStatus.CREATED);
    }
    @Secured(AuthorityConstant.ROLE_ADMIN)
    @DeleteMapping("/deleteUser/{username}")
    public ResponseEntity<String> deleteUser(@PathVariable("username") String username) {
        User user = userService.findByUsername(username);
        if(user!=null) {
            userService.delete(user);
            return new ResponseEntity<>(username+" is deleted ", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(username+ " is not found ", HttpStatus.NOT_FOUND);
        }
    }
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("/findByUserName/{username}")
    public ResponseEntity<UserDTO> findByUserName(@PathVariable("username") String username) {
        UserDTO user = userService.findOneByUsername(username);
        if(user!=null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("/findByUserId/{id}")
    public ResponseEntity<UserDTO> findByUserName(@PathVariable("id") Integer id) {
        UserDTO user = userService.findById(id);
        if(user!=null) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("/addFavorit/{idUser}/{idVideo}")
    public ResponseEntity<String> addFavorit(@PathVariable ("idUser") Integer idUser, @PathVariable ("idVideo") Integer idVideo) {
        Video video=videoService.findById(idVideo);
        UserDTO user = utilisateurService.findById(idUser);
        utilisateurService.addVideoToFavorit(user,idVideo );
        return new ResponseEntity<>("video added to favorit", HttpStatus.OK);
    }


    @Secured({AuthorityConstant.ROLE_USER, AuthorityConstant.ROLE_ADMIN})
    @GetMapping("removeFromFavorit/{idUser}/{idVideo}")
    public  ResponseEntity<String> removeFromFavorit(@PathVariable ("idUser") Integer idUser, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UserDTO user = utilisateurService.findById(idUser);
        utilisateurService.removeVideoFromFavorit(user,id );
        return new ResponseEntity<>("video removed from favorit", HttpStatus.OK);
    }















    // TODO: 16/05/2023  : passer les requettes suivantes en api rest selon besoin


    @RequestMapping(value = "/deleteFavorit/{idVideo}", method = RequestMethod.GET)
    public String deleteFavorit(Model model,HttpServletRequest request, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UserDTO user =(UserDTO) request.getSession().getAttribute("utilisateur");
        utilisateurService.removeVideoFromFavorit(user,id );
        uploadAllList(request);
        return "jsp/showVideos";
    }

    @RequestMapping("/showFavorit")
    public String showFavorit(HttpServletRequest request) {
        List<Video> favoritList = utilisateurService.userFavorit(((UserDTO)request.getSession().getAttribute("utilisateur")).getIdUser());
        request.getSession().setAttribute("favoritList", favoritList);
        return "jsp/favorit";
    }

    private void uploadAllList(HttpServletRequest request) {
        List<Video> listVideos = videoService.setByteMiniatureForListVideos(videoService.findAll());
        request.getSession().setAttribute("listVideos", listVideos);

        List<Video> favoritList = utilisateurService.userFavorit(((UserDTO)request.getSession().getAttribute("utilisateur")).getIdUser());
        request.getSession().setAttribute("favoritList", favoritList);
    }

    @RequestMapping(value = "/fav/deleteFavorit/{idVideo}", method = RequestMethod.GET)
    public String deleteFromFavorit(Model model,HttpServletRequest request, @PathVariable ("idVideo") Integer id) {
        Video video=videoService.findById(id);
        UserDTO user =(UserDTO) request.getSession().getAttribute("utilisateur");
        utilisateurService.removeVideoFromFavorit(user,id );
        uploadAllList(request);
        return "jsp/favorit";
    }


}
