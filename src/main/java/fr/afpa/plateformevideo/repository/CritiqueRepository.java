package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Critique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CritiqueRepository extends JpaRepository<Critique, Integer> {

    public List<Critique> critiquesByVideo(int idVideo) ;

    @Query("select AVG(a.score) from Critique a where a.video.idVideo = ?1")
    public Double scoreByVideo(int idVideo) ;




}
