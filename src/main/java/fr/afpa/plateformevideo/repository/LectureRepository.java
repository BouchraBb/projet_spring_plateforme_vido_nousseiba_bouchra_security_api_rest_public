package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LectureRepository extends JpaRepository<Lecture, Integer> {
    public List<Lecture> allUserVideoLecture(Integer idUser);

    public Lecture userVideoLecture(Integer idUser, Integer idVideo);
}

