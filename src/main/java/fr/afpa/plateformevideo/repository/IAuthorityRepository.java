package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAuthorityRepository extends JpaRepository<Authority, String> {
}
