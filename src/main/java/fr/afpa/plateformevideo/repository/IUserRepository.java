package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.dto.UserDTO;
import fr.afpa.plateformevideo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
     User findByEmailAndPassword(String email, String password);
    UserDTO findOneByUsername(String username);

}
