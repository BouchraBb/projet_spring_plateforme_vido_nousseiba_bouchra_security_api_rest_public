package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.MotCle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MotCleRepository extends JpaRepository<MotCle, Integer> {
}
