package fr.afpa.plateformevideo.repository;

import fr.afpa.plateformevideo.entity.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoRepository extends JpaRepository<Video, Integer> {


    public List<Video> videosSimilaires(int idVideo) ;
    public List<Video> findFavoritesByUserId( int userId );
}
