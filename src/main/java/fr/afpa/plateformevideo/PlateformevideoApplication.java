package fr.afpa.plateformevideo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlateformevideoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlateformevideoApplication.class, args);
	}

}
