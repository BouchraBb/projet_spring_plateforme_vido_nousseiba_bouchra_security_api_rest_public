package fr.afpa.plateformevideo.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 */
public class UserUtil {

    /**
     *
     * @return authenticated user
     */

    public static  UserDetails getCurrentUser(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = null;
        if(authentication != null)
            userDetails =   (UserDetails) authentication.getPrincipal();

        return userDetails;

    }
}
