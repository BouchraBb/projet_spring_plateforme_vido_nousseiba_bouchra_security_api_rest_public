package fr.afpa.plateformevideo.security;

public class AuthorityConstant {

    public final static String ROLE_ADMIN = "ROLE_ADMIN";
    public final static String ROLE_USER = "ROLE_USER";

}